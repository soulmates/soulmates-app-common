from __future__ import unicode_literals

from django.db import models

# Create your models here.


class SeoFieldsMixin(models.Model):
    seo_title = models.CharField(blank=True, max_length=255)
    seo_keywords = models.TextField(blank=True)
    seo_description = models.TextField(blank=True)
    
    def get_seo_title(self):
        return self.seo_title if self.seo_title else self.title
    
    class Meta:
        abstract = True